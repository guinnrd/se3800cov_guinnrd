all: hexiConverter hexTest
		g++ -std=c++14 -Wall --coverage hexiConverter.o HexString.o  main.cpp


hexiConverter.o: hexiConverter.h hexiConverter.cpp HexString.h
	g++ -std=c++14 -Wall --coverage -c hexiConverter.cpp

HexString.o: HexString.h HexString.cpp 
	g++ -std=c++14 -Wall --coverage -c HexString.cpp

hexTest.o: hexTest.cpp 
	g++ -std=c++14 -Wall --coverage -c hexTest.cpp 

main.o: hexiConverter.h main.cpp 
	g++ -std=c++14 -Wall --coverage -c main.cpp
    
hexiConverter: hexiConverter.o HexString.o main.o
	g++ -std=c++14 -Wall --coverage hexiConverter.o HexString.o  main.o -o hexiConverter 

hexTest: hexTest.o hexiConverter.o HexString.o
	g++ -pthread -std=c++14 -Wall --coverage hexTest.o hexiConverter.o HexString.o -lgtest_main -lgtest -lpthread -o hexTest 

coverage: hexTest
	mkdir -p code_coverage_report
	-./hexTest
	lcov --directory ./ --capture --output-file ./code_coverage.info -rc lcov_branch_coverage=1
	genhtml code_coverage.info --branch-coverage --output-directory ./code_coverage_report/

clean:
	rm -f *.o *.gcda *.gcno code_coverage.info hexiConverter hexTest
	rm -rf code_coverage_report